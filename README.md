# CentOS 7.x SSH 9.3p2 与 OpenSSL 1.1.1u RPM 升级脚本

## 简介

本仓库提供了一个用于升级 CentOS 7.x 系统中 OpenSSH 和 OpenSSL 的脚本。该脚本采用 RPM 包形式，可以一键快速升级 OpenSSH 至 9.3p2 版本，以及 OpenSSL 至 1.1.1u 版本。升级过程无需每台服务器单独进行编译，大大简化了升级操作。

## 特点

1. **同时升级 OpenSSH 与 OpenSSL**：采用 RPM 包形式，一键快速升级版本，无需每台服务器单独进行编译。
2. **隐藏 OpenSSH 版本号**：升级后，OpenSSH 的版本号将被隐藏，增加安全性。
3. **保留 `scp` 与 `ssh-copy-id` 命令**：升级过程中，这些常用命令将被保留，不影响日常使用。

## 安装步骤

1. 下载本仓库中的 `upgrade_ssl_ssh.sh` 脚本。
2. 在目标服务器上执行以下命令进行安装：

   ```bash
   bash upgrade_ssl_ssh.sh
   ```

## 验证版本

安装完成后，可以通过以下命令验证 OpenSSL 和 OpenSSH 的版本：

- **验证 OpenSSL 版本**：

  ```bash
  openssl version
  ```

  输出应为：

  ```
  OpenSSL 1.1.1u  30 May 2023
  ```

- **验证 OpenSSH 版本**：

  ```bash
  ssh -V
  ```

  输出应为：

  ```
  OpenSSH_9.3p2 OpenSSL 1.1.1u  30 May 2023
  ```

## 注意事项

- 在执行升级脚本前，请确保备份重要数据，以防升级过程中出现意外情况。
- 升级过程中可能会重启 SSH 服务，请确保在合适的时间进行操作，避免影响业务。

## 贡献

欢迎提交 Issue 或 Pull Request 来改进本脚本。